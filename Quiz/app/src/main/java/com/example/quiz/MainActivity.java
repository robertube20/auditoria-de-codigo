package com.example.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnAdmin, btnPrueba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        btnAdmin = findViewById( R.id.btnAdmin );
        btnPrueba = findViewById( R.id.btnPrueba );


        btnAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainActivity.this, PanelAdmin.class );
                startActivity( intent );
            }
        } );

        btnPrueba.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainActivity.this, ShowQuestion.class );
                startActivity( intent );
            }
        } );

    }
}
