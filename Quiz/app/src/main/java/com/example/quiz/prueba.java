package com.example.quiz;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.quiz.utilidades.Utilidades.CAMPO_ID;
import static com.example.quiz.utilidades.Utilidades.CAMPO_NOMBRE;
import static com.example.quiz.utilidades.Utilidades.CAMPO_PASS;
import static com.example.quiz.utilidades.Utilidades.TABLA_ADMIN;

public class prueba extends AppCompatActivity {

    EditText searchId, searchNombre, searchPass;
    Button Search;
    SQLiteConexionHelper conexionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_prueba );

        conexionHelper = new SQLiteConexionHelper( getApplicationContext(),"bd_admin", null, 1 );

        searchId = findViewById( R.id.edtSearch );
        searchNombre = findViewById( R.id.edtName );
        searchPass = findViewById( R.id.edtPassw );
        Search = findViewById( R.id.btnBuscar );

        Search.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    consultar();
                }
            } );
    }

    private void consultar() {
        SQLiteDatabase db = conexionHelper.getReadableDatabase();
        String[] parametros = {searchId.getText().toString()};
        String[] campos = {CAMPO_NOMBRE, CAMPO_PASS};

        try{
            Cursor cursor = db.query( TABLA_ADMIN, campos,CAMPO_ID + "=?", parametros, null,null,null );
            cursor.moveToFirst();

            searchNombre.setText( cursor.getString( 0 ) );
            searchPass.setText( cursor.getString( 1 ) );

            cursor.close();

        }catch (Exception x){
            showMessage("El documento no existe");
            limpiar();
        }


    }

    private void limpiar() {
        searchNombre.setText( "" );
        searchPass.setText( "" );
    }

    private void showMessage(String message) {
        Toast.makeText( this, message, Toast.LENGTH_SHORT ).show();
    }
}
