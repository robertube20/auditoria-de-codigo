package com.example.quiz;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.quiz.utilidades.Utilidades.CREAR_TABLA_ADMIN;
import static com.example.quiz.utilidades.Utilidades.CREAR_TABLA_QUESTION;
import static com.example.quiz.utilidades.Utilidades.TABLA_ADMIN;
import static com.example.quiz.utilidades.Utilidades.TABLA_QUIZ;

public class SQLiteConexionHelper extends SQLiteOpenHelper {
    public SQLiteConexionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super( context, name, factory, version );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL( CREAR_TABLA_ADMIN );
        db.execSQL( CREAR_TABLA_QUESTION );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL( "DROP TABLE IF EXISTS "+ TABLA_ADMIN );
        db.execSQL( "DROP TABLE IF EXISTS "+ TABLA_QUIZ );

        onCreate( db );
    }
}
