package com.example.quiz.entidades;

public class QuizEvents {

    int idQ;
    String NPregunta;
    String Pregunta;

    public QuizEvents(int idQ, String NPregunta, String pregunta) {
        this.idQ = idQ;
        this.NPregunta = NPregunta;
        Pregunta = pregunta;
    }

    public int getIdQ() {
        return idQ;
    }

    public void setIdQ(int idQ) {
        this.idQ = idQ;
    }

    public String getNPregunta() {
        return NPregunta;
    }

    public void setNPregunta(String NPregunta) {
        this.NPregunta = NPregunta;
    }

    public String getPregunta() {
        return Pregunta;
    }

    public void setPregunta(String pregunta) {
        Pregunta = pregunta;
    }
}
