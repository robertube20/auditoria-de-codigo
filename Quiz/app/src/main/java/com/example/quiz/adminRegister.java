package com.example.quiz;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.quiz.utilidades.Utilidades.CAMPO_ID;
import static com.example.quiz.utilidades.Utilidades.CAMPO_NOMBRE;
import static com.example.quiz.utilidades.Utilidades.CAMPO_PASS;
import static com.example.quiz.utilidades.Utilidades.TABLA_ADMIN;

public class adminRegister extends AppCompatActivity {

    EditText campoID, campoNombre, campoPass;
    Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_admin_register );

        campoID = findViewById( R.id.EdtId );
        campoNombre = findViewById( R.id.edtNombre );
        campoPass = findViewById( R.id.edtContra );
        btnRegistrar = findViewById( R.id.btnRegistro );

        btnRegistrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarAdmin();
            }
        } );
    }

    private void registrarAdmin(){
        SQLiteConexionHelper conexionHelper = new SQLiteConexionHelper( getApplicationContext(),"bd_admin",null,1 );
        SQLiteDatabase db = conexionHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put( CAMPO_ID, campoID.getText().toString());
        values.put( CAMPO_NOMBRE, campoNombre.getText().toString());
        values.put( CAMPO_PASS, campoPass.getText().toString());

        Long idResultado = db.insert( TABLA_ADMIN, CAMPO_ID, values );

        showMessage("Se ha registrado con exito: "+ idResultado);

        db.close();
    }

    private void showMessage(String message) {
        Toast.makeText( this, message, Toast.LENGTH_LONG ).show();
    }


}
