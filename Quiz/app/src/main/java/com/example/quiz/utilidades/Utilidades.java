package com.example.quiz.utilidades;

public class Utilidades {
    //Constantes campos tabla admin
    public static final String TABLA_ADMIN= "admin";
    public static final String CAMPO_ID= "id";
    public static final String CAMPO_NOMBRE= "nombre";
    public static final String CAMPO_PASS= "pass";

    public static final String CREAR_TABLA_ADMIN = "CREATE TABLE "+TABLA_ADMIN+"" +
            "("+CAMPO_ID+" INTEGER, "
            +CAMPO_NOMBRE+" TEXT, "
            +CAMPO_PASS+" TEXT)";

    //Constantes para tabla question
    public static final String TABLA_QUIZ= "quiz";
    public static final String CAMPO_ID_Q= "id";
    public static final String CAMPO_N_QUESION= "npregunta";
    public static final String CAMPO_QUESTION= "pregunta";
    public static final String CAMPO_CATEGORY= "categoria";
    public static final String CAMPO_DECRIPCION= "descripcion";

    public static final String CREAR_TABLA_QUESTION = "CREATE TABLE "+TABLA_QUIZ+"" +
            "("+CAMPO_ID_Q+" INTEGER PRIMARY KEY AUTOINCREMENT  , "
            +CAMPO_N_QUESION+" TEXT, "
            +CAMPO_QUESTION+" TEXT,"
            +CAMPO_CATEGORY+" TEXT,"
            +CAMPO_DECRIPCION+" TEXT)";
}
