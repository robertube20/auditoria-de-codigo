package com.example.quiz;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.quiz.utilidades.Utilidades.CAMPO_CATEGORY;
import static com.example.quiz.utilidades.Utilidades.CAMPO_DECRIPCION;
import static com.example.quiz.utilidades.Utilidades.CAMPO_ID_Q;
import static com.example.quiz.utilidades.Utilidades.CAMPO_N_QUESION;
import static com.example.quiz.utilidades.Utilidades.CAMPO_QUESTION;
import static com.example.quiz.utilidades.Utilidades.TABLA_QUIZ;

public class RegisterQuesion extends AppCompatActivity {

    EditText campoNPregunta;
    EditText campoPregunta;
    EditText campoDescripcion;
    Spinner campoCategory;
    String itemSelected;
    Button btnAddQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register_quesion );

        campoNPregunta = findViewById( R.id.edtIdNQ );
        campoPregunta = findViewById( R.id.edtQ );
        campoDescripcion = findViewById( R.id.edtD );
        btnAddQuestion = findViewById( R.id.AddQuestion );
        campoCategory = findViewById( R.id.spnCategory );

        ArrayList<String> arrayList = new ArrayList<>(  );
        arrayList.add( "Adecuacion funcional" );
        arrayList.add( "Eficiencia de Desempeño" );
        arrayList.add( "Adecuacion Compatibilidad" );
        arrayList.add( "Usabilidad" );
        arrayList.add( "Fiabilidad" );
        arrayList.add( "Seguridad" );
        arrayList.add( "Manteniabilidad" );
        arrayList.add( "Portabilidad" );

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        campoCategory.setAdapter(arrayAdapter);
        campoCategory.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemSelected = parent.getItemAtPosition( position ).toString();
                showMessage("Seleccionado: "  + itemSelected );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );


        btnAddQuestion.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerQuestion();
            }
        } );
    }

    private void registerQuestion() {
        SQLiteConexionHelper conexionHelper = new SQLiteConexionHelper( getApplicationContext(),"bd_admin",null,1 );
        SQLiteDatabase db = conexionHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put( CAMPO_N_QUESION, campoNPregunta.getText().toString() );
        values.put( CAMPO_QUESTION, campoPregunta.getText().toString() );
        values.put( CAMPO_CATEGORY, itemSelected );
        values.put( CAMPO_DECRIPCION, campoDescripcion.getText().toString() );

        Long idResultado = db.insert( TABLA_QUIZ, CAMPO_ID_Q, values );

        showMessage( "Se guardo: " + idResultado );

        db.close();

    }


    private void showMessage(String message) {
        Toast.makeText( this, message, Toast.LENGTH_LONG ).show();
    }
}
