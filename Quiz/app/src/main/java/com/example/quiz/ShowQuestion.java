package com.example.quiz;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.quiz.utilidades.Utilidades.CAMPO_CATEGORY;
import static com.example.quiz.utilidades.Utilidades.CAMPO_DECRIPCION;
import static com.example.quiz.utilidades.Utilidades.CAMPO_ID_Q;
import static com.example.quiz.utilidades.Utilidades.CAMPO_N_QUESION;
import static com.example.quiz.utilidades.Utilidades.CAMPO_QUESTION;
import static com.example.quiz.utilidades.Utilidades.TABLA_QUIZ;

public class ShowQuestion extends AppCompatActivity {
    TextView npregunta;
    TextView pregunta;
    TextView txtCategory;
    TextView txtCountQuestion;
    TextView txtPercent;
    TextView txtDescipction;
    Button Cumple;
    Button noCumple;
    Button noAplica;
    Button btnResult;
    SQLiteConexionHelper conexionHelper;
    int cumpleCount;
    int noCumpleCount;
    int noAplicaCount;
    int count = 1;
    String tot;


    float porcen;
    String porci = "0.00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_show_question );

        conexionHelper = new SQLiteConexionHelper( getApplicationContext(),"bd_admin", null, 1 );

        npregunta = findViewById( R.id.txvNumQuestion );
        pregunta= findViewById( R.id.txvQuestion );
        txtPercent = findViewById( R.id.txtPercent );
        Cumple = findViewById( R.id.btnSiCumple );
        noCumple = findViewById( R.id.btnNoCumple );
        noAplica = findViewById( R.id.btnNoAplica );
        btnResult = findViewById( R.id.result );
        txtCategory = findViewById( R.id.txtCategory );
        txtCountQuestion = findViewById( R.id.txtCountQuestion );
        txtDescipction = findViewById( R.id.txtDescipction );

        txtPercent.setText( "Aprobacion actual " + porci + "%");

        consultar();
        lastRegister();

        Cumple.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                cumpleCount++;
                consultar();
                showPercent();
                lastRegister();
            }
        } );

        noCumple.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                noCumpleCount++;
                consultar();
                showPercent();
                lastRegister();
            }
        } );

        noAplica.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                noAplicaCount++;
                consultar();
                showPercent();
                lastRegister();
            }
        } );

        btnResult.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastRegister();
            }
        } );

    }


    @SuppressLint("SetTextI18n")
    private void lastRegister() {
        String sql = "select * from " + TABLA_QUIZ;
        SQLiteDatabase db = conexionHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery( sql,null );

        String id = "";

        if(cursor.moveToFirst()){
            do{
                id = cursor.getString( 0 );
                tot = cursor.getString( 0 );
            }while (cursor.moveToNext());
        }
        cursor.close();

        if(count > Integer.parseInt( id )){
            btnResult.setVisibility( View.VISIBLE );

            Cumple.setVisibility( View.GONE );
            noCumple.setVisibility( View.GONE );
            noAplica.setVisibility( View.GONE );
            txtCategory.setVisibility( View.GONE );
            txtDescipction.setVisibility( View.GONE );

            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            builder.setTitle( "Resultados" );

            LinearLayout linearLayout = new LinearLayout( this );

            final TextView resultado = new TextView( this );

            int res = count - 1;

            float suma = noCumpleCount + noAplicaCount + cumpleCount;

            float porcentaje;
            String porciento = null;


                porcentaje = (float) ((cumpleCount / suma) * 100);
                porciento = String.valueOf( Float.toString( porcentaje ).length() );
                if(porciento.length() > 3) {
                    porciento = Float.toString( porcentaje ).substring( 0, 4 );
                }else{
                    porciento = Float.toString( porcentaje ).substring( 0, 3 );
                }



            resultado.setText( "Se han contestado: " + res + " / " + id +" preguntas." + '\n' + '\n' +
                    "Se contestaron: " + cumpleCount + " cumplidas. " + '\n' +
                    "Se contestaron: " + noCumpleCount + " no cumplidas." + '\n' +
                    "Se contestaron: " + noAplicaCount + " no aplica." + '\n' + '\n' +
                    "Con una aprobación de: " + porciento + "%");

            txtPercent.setText( "Aprobacion actual " + porciento + "%");

            resultado.setEms( 16 );

            linearLayout.addView( resultado );

            linearLayout.setPadding( 16,16,16,16 );
            builder.setView( linearLayout );

            //Creamos boton cancelar
            builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Cerrar la ventana
                    dialog.dismiss();
                }
            });

            builder.create().show();

        }else{
          //  showMessage( "Ocurrió un error inesperado" );
        }
    }

    private void consultar() {
        SQLiteDatabase db = conexionHelper.getReadableDatabase();
        String[] parametros = {String.valueOf( count )};
        String[] campos = {CAMPO_N_QUESION, CAMPO_QUESTION, CAMPO_CATEGORY, CAMPO_DECRIPCION};

        try {

            Cursor cursor = db.query( TABLA_QUIZ, campos, CAMPO_ID_Q + "=?", parametros, null, null, null );
            cursor.moveToFirst();


            npregunta.setText( cursor.getString( 0 ) );
            pregunta.setText( cursor.getString( 1 ) );

            String s = "Categoría de evaluación: " + "<b> "+cursor.getString( 2 )+" </b>";
            txtCategory.setText( Html.fromHtml( s ) );

            txtCountQuestion.setText( cursor.getString( 0 ) + " / 30" );

            String d = "<b>Descripcion de la categoria: </b>"+ cursor.getString( 3 );
            txtDescipction.setText( Html.fromHtml( d ) );



        }catch (Exception x){
   //         showMessage( "No existen datos" );
            limipar();
        }

    }

    private void showPercent() {

        double suma = 30;
        if(cumpleCount != 0) {
            porcen = (float) (cumpleCount / suma) * 100;
            porci = Float.toString( porcen ).substring( 0, 4 );

            txtPercent.setText( "Aprobacion actual " + porci + "%" );
        }else{
            txtPercent.setText( "Aprobacion actual " + porci + "%" );
        }

    }


    private void limipar() {
        npregunta.setText( "" );
        pregunta.setText( "" );
    }

    private void showMessage(String message) {
        Toast.makeText( this, message, Toast.LENGTH_SHORT ).show();
    }
}
