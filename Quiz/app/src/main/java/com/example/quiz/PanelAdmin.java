package com.example.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PanelAdmin extends AppCompatActivity {

    Button btnQuestion, btnPrueba, btnAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_panel_admin );

        btnQuestion = findViewById( R.id.btnPreguntas );
        btnPrueba = findViewById( R.id.btnPr );
        btnAdmin = findViewById( R.id.btnPr2 );

        btnQuestion.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( PanelAdmin.this, RegisterQuesion.class );
                startActivity( intent );
            }
        } );

        btnPrueba.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( PanelAdmin.this, prueba.class );
                startActivity( intent );
            }
        } );

        btnAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( PanelAdmin.this, adminRegister.class );
                startActivity( intent );
            }
        } );

    }
}
